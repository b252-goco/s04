-- [SECTION] Advanced Selects

-- NOT --
-- To exclude records, use the '!' which is similar to a NOT operator
SELECT * FROM songs WHERE id != 11;

-- LESS THAN --
SELECT * FROM songs WHERE id < 11;

-- GREATEAR THAN --
SELECT * FROM songs WHERE id > 11;

-- OR --
-- the first to satisfy will show in the result
SELECT * FROM songs WHERE id = 1 OR id = 3 or id = 5;

-- IN --
-- the first to satisfy will show in the result
SELECT * FROM songs WHERE id IN (1,2,3);
SELECT * FROM songs WHERE genre IN ("Pop", "K-pop");


-- Combining Conditions --
-- AND --
SELECT * FROM songs WHERE album_id = 4 and id < 8;

-- Finding Partial Matches --
SELECT * FROM songs WHERE song_name LIKE "%a"; -- finding matches with 'a' at the END
SELECT * FROM songs WHERE song_name LIKE "a%"; -- finding matches with 'a' at the START
SELECT * FROM songs WHERE song_name LIKE "%a%"; -- finding matches with 'a' at the ANY POSITION

-- Sorting Records --
SELECT * FROM songs ORDER BY song_name ASC;
SELECT * FROM songs ORDER BY song_name DESC;

-- [SECTION] Table Joins
SELECT * FROM artists
	JOIN albums on artists.id = albums.artist_id;

SELECT * FROM artists
	JOIN albums ON artists.id = albums.artist_id
	JOIN songs ON albums.id = songs.album_id;

SELECT artists.name, albums.album_title FROM artists
	JOIN albums on artists.id = albums.artist_id;